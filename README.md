<p align='center'>
<img src="https://www.majemedia.com/content/uploads/2016/04/maje_horiz_RGB.png" alt='Maje Media Logo' title="Maje Media LLC">
</p>

# Case Insensitive Uploads

A WordPress plugin to handle 404's of upload filenames that are incorrectly cased

